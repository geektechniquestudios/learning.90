package com.geektechnique.mvcthymeleafform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcThymeleafFormApplication {

    public static void main(String[] args) {
        SpringApplication.run(MvcThymeleafFormApplication.class, args);
    }

}
